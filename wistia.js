class ProgressSaver {

    monitor() {
        setInterval( () => {
            this.updateLocal();
        }, 5000);
    }

    updateLocal() {
        let time = this.getTime();
        localStorage.setItem('edools_current_video_time', time);
    }

    updateRemote() {
        let request = $.ajax({
            method: 'PATCH',
            url: window.CORE_HOST + '/lessons_progresses/' + lessonProgress.id,
            headers : {
              'Authorization' : 'Token token=' + player.apiKey
            },
            data: JSON.stringify({
                lesson_progress: {
                    current_video_time: this.getTime()
                }
            }),
            contentType: 'application/json'
        });

        request.success(function () {

        });

        request.error(function (data) {

        });
    }

    loadTime() {
        let time = localStorage.getItem('edools_current_video_time');
        this.setTime(time);
    }

};

/*
    Wistia Progress Saver Class
    
    We use this class to keep saving the video progress. This class can be used 
    as an example to create new ones. Check the method docs
*/

class WistiaProgressSaver extends ProgressSaver {

    constructor(id) {
        super();
        this.player = Wistia.api(id);
    }

    setTime(time) {
        this.player.time(time);
    }

    getTime() {
        return this.player.time();
    }
}