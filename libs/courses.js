var moodle = require('./moodle.js');
var jsonfile = require('jsonfile');
var async = require('async');
var _ = require('underscore');

get_courses_by_id = function (userId, callback) {
	moodle.call_moodle("core_enrol_get_users_courses", {userid: userId}, callback);
}
exports.get_courses_by_id = get_courses_by_id;

get_course_content = function (courseId, callback) {
	moodle.call_moodle("mod_quiz_get_quizzes_by_courses", {courseid: courseId}, callback);
}
exports.get_course_content = get_course_content;

generate_files_per_course = function () {
	var coursesData = jsonfile.readFileSync('./docs/courses_list.json');
	
	coursesData.forEach( course => {
		get_course_content(course.id, (details) => {
			jsonfile.writeFileSync('./docs/courses/' + course.id + '.json', {course: course, details: details});
		});
	});
}
exports.generate_files_per_course = generate_files_per_course;

format_lessons = function (modules) {
	return _.map(modules, (lesson, index) => {
		return {
			oldId: lesson.id,
			order: index,
			content_id: lesson.id,
			content_type: "Lesson",
			downloadable: null,
			available: null
		}
	});
}

// 78, 121
courses_to_export = [80, 81, 82, 83, 84, 87, 88, 89, 90, 91, 92, 93, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 118, 126, 127, 133, 141];

export_courses = function () {
	data = [];

	async.each(courses_to_export, (course, callback) => {
		rawData = jsonfile.readFileSync('./docs/courses/' + course + '.json');
		
		formatted_course = format_course(rawData);
		data.push(formatted_course);

		callback();
		
	}, (err) => {
		jsonfile.writeFileSync('./files/coursesData.json', {courses: data});
	});

	
}
exports.export_courses = export_courses;

format_modules = function (details) {
	return _.map(details, (section, index) => {
		return {
			oldId: section.id,
			order: index,
			name: (index === 0 || section.name.trim().length === 0) ? "Introdução" : section.name,
			description: null,
			available: null,
			course_modules: [],
			course_contents: format_lessons(section.modules)
		}
	});
}

format_course = function (item) {
	return {  
         "oldId": item.course.id,
         "name": item.course.fullname,
         "duration":"",
         "image_url":"https://s3.amazonaws.com/core_prod/org-116%2Fschool-423%2Fab9c594a4cc14b04e1565dbe2dcc442f%2Fcurso500x287-+intermediarioNOVO.png",
         "code": "",
         "description": item.details[0].summary,
         "ready":true,
         "course_modules": format_modules(item.details)
      };
}

exports.format_course = format_course;











