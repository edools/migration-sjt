var moodle = require('./moodle.js');
var jsonfile = require('jsonfile');
var async = require('async');
var _ = require('underscore');
var fs = require('fs');
var xml2js = require('xml2js');

lesson_to_quiz = function (lesson_id) {
	var parser = new xml2js.Parser();
	fs.readFile('./docs/lessons/lesson_' + lesson_id + '.xml', function(err, data) {
	    parser.parseString(data, function (err, result) {
	        console.dir(result);
	        jsonfile.writeFileSync('./docs/lessons/lesson_' + lesson_id + '.json', result);
	        console.log('Done');
	    });
	});
}
exports.lesson_to_quiz = lesson_to_quiz;

get_webcast_id = function (lesson, course) {
	var parser = new xml2js.Parser();
	fs.readFile('./docs/courses/' + course.id + '/activities/webcast_' + lesson.id + '/webcast.xml', function (err, data) {
		parser.parseString(data, function (err, result) {
	        if (result && result.activity) {
	        	console.log(result.activity.webcast[0].webcastid[0]+ ',');
	        	// jsonfile.writeFileSync('./docs/lessons/lesson_' + lesson_id + '.json', result);
	        }
	    });
	});
}

courses_to_export = [80, 81, 82, 83, 84, 87, 88, 89, 90, 91, 92, 93, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 118, 126, 127, 133, 141];

export_lessons = function () {
	data = [];

	async.each(courses_to_export, (course, callback) => {
		rawData = jsonfile.readFileSync('./docs/courses/' + course + '.json');
		
		formatted_lessons = check_lessons(rawData);
		data.push(formatted_lessons);

		callback();
		
	}, (err) => {
		jsonfile.writeFileSync('./files/lessonsData.json', {lessons: _.flatten(data)});
	});

	
}
exports.export_lessons = export_lessons;

lesson_type = function (course_id) {
	total = 0;
	types = []

	data = jsonfile.readFileSync('./docs/courses/' + course_id + '.json');
	check = check_lessons(data);
	videos = _.filter(check, i => {
		types.push(i.type)
		return i.type == "lesson"
	})
	total = total + videos.length;
	console.log('Curso ' + course_id + ': ' + videos.length);

	group = _.groupBy(types);

	for (key in group) {
		console.log(key, group[key].length)
	}
}
exports.lesson_type = lesson_type;

create_lessons = function (modules, course) {
	return _.map(modules, (lesson, index) => {
		return {
			oldId: lesson.id,
			title: lesson.name,
			type: "Lesson",
			available: null,
			description: null,
			min_grade: null,
			unlock_certificate: null,
			media_id: 77023,
			release_at: null,
			release_after: null
		}
	});
}
exports.create_lessons = create_lessons;

check_lessons = function (course) {
	return _.flatten(_.map(course.details, (sections) => {
		return create_lessons(sections.modules, course.course);
	}));
}
exports.check_lessons = check_lessons;