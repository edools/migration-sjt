var moodle_client = require("moodle-client");

function init_moodle(callback) {
	moodle_client.init({
	    wwwroot: "http://ead.sjteducacaomedica.com.br/",
	    username: "sjt",
	    password: "educacaomedica"

	}).then(function(client) {
	    callback(client);

	}).catch(function(err) {
	    console.log("Unable to initialize the client: " + err);
	});
}

function call_moodle(ws, params, callback) {
	var call = {
		wsfunction: ws
	};

	if (params)
		call.args = params;

	init_moodle( function (client) {
		client.call(call).then( function (info) {
			callback(info);
		});
	});
}

exports.call_moodle = call_moodle;