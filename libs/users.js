var moodle = require('./moodle.js');
var jsonfile = require('jsonfile');
var async = require('async');
var _ = require('underscore');
var fs = require('fs');

get_users = function (callback) {
	moodle.call_moodle("core_user_get_users_by_field", {email: 'luana@edools.com'}, callback);
}
exports.get_users = get_users;

get_user_by_id = function (userId, callback) {
	moodle.call_moodle("core_user_get_users_by_id", {userids: [userId]}, callback);
}
exports.get_user_by_id = get_user_by_id;

get_users_from_course = function (courseId, callback) {
	moodle.call_moodle("core_enrol_get_enrolled_users", {courseid: courseId}, callback);
}
exports.get_users_from_course = get_users_from_course;

read_local_users = function () {
	var usersData = jsonfile.readFileSync('./docs/users_list.json');
	var newUsers = [];

	async.eachLimit(usersData, 25, (user, callback) => {

		fs.exists('./docs/users/' + user.id + '.json', exists => {
			if (exists) {
				return callback();
			} else {
				users.get_user_by_id(user.id, (data) => {
					newUsers.push(data[0]);
					jsonfile.writeFileSync('./docs/users/' + user.id + '.json', data[0]);
					callback();
				});
			}
		});
		

	}, (err) => {
		console.log(newUsers);
		console.log('Done');
	});
}

generate_user_files = function () {
	var usersData = jsonfile.readFileSync('./docs/users_list.json');
	var newData = [];

	async.eachLimit(usersData, 5, (user, callback) => {
		fs.exists('./docs/users/' + user.id + '.json', exists => {
			
			if (!exists)
				return callback();

			userData = jsonfile.readFileSync('./docs/users/' + user.id + '.json');
			userData = format_user(userData);
			newData.push(userData);
			callback();
		});
	}, (err) => {
		jsonfile.writeFileSync('./files/usersData.json', {users: newData});
	});
}
exports.generate_user_files = generate_user_files;

format_user = function (user) {
	return {  
		"oldId": user.id,
		"first_name": user.firstname,
		"last_name": user.lastname,
		"email": user.email,
		"username": null,
		"type": "Student",
		"cpf": null,
		"rg": null,
		"phone": user.profile_field_Celular,
		"extra_phone": user.profile_field_Residencial,
		"skype": user.skype,
		"twitter":null,
		"facebook":null,
		"company_name":null,
		"company_position":null,
		"biography":null,
		"cover_image_url":"https://cdn.edools.com/assets/images/users/default.jpeg",
		"last_sign_in_at":null,
		"born_at": null,
		"enrollments": format_enrollments(user.enrolledcourses),
		"addresses":[]
	}
}

format_enrollments = function (enrollments) {
	if (!enrollments || !_.isArray(enrollments))
		return [];
	return _.map(enrollments, item => {
		return {
			"oldId": 250358,
			"school_product_id": item.id,
			"status": "active",
			"unlimited": false,
			"available_until":"2016-12-31T19:30:26.603Z"
		};
	});
}